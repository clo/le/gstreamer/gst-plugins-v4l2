/*
* Copyright (C) 2015 STMicroelectronics SA
*
* Author: <fabien.dessenne@st.com> for STMicroelectronics.
*
* Bufferpool using DMABUF allocator and V4L2 output
* The buffers of this pool are shared between v4l2trans and the upstream element
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <fcntl.h>
#include <string.h>

#include "gstv4l2outbufpool.h"

GST_DEBUG_CATEGORY_EXTERN (gst_v4l2trans_debug);
#define GST_CAT_DEFAULT gst_v4l2trans_debug

GType
gst_v4l2trans_meta_api_get_type (void)
{
  static volatile GType type;
  static const gchar *tags[] = { "memory", NULL };

  if (g_once_init_enter (&type)) {
    GType _type = gst_meta_api_type_register ("GstV4L2TransMetaAPI", tags);
    g_once_init_leave (&type, _type);
  }
  return type;
}

const GstMetaInfo *
gst_v4l2trans_meta_get_info (void)
{
  static const GstMetaInfo *meta_info = NULL;

  if (g_once_init_enter (&meta_info)) {
    const GstMetaInfo *meta =
        gst_meta_register (gst_v4l2trans_meta_api_get_type (),
        "GstV4L2TransMeta", sizeof (GstV4L2TransMeta),
        (GstMetaInitFunction) NULL, (GstMetaFreeFunction) NULL,
        (GstMetaTransformFunction) NULL);
    g_once_init_leave (&meta_info, meta);
  }
  return meta_info;
}

#define gst_v4l2_out_buf_pool_parent_class parent_class
G_DEFINE_TYPE (GstV4L2OutBufPool, gst_v4l2_out_buf_pool, GST_TYPE_BUFFER_POOL);

static const gchar **
gst_v4l2_out_buf_pool_get_options (GstBufferPool * pool)
{
  static const gchar *options[] = { GST_BUFFER_POOL_OPTION_VIDEO_META,
    GST_BUFFER_POOL_OPTION_VIDEO_ALIGNMENT, NULL
  };

  return options;
}

static GstFlowReturn
gst_v4l2_out_buf_pool_alloc_buffer (GstBufferPool * bpool, GstBuffer ** buffer,
    GstBufferPoolAcquireParams * params)
{
  GstV4L2OutBufPool *pool = GST_V4L2_OUT_BUF_POOL (bpool);
  GstBuffer *newbuf;
  GstV4L2TransMeta *meta;
  guint index, size, min_buffers, max_buffers;
  struct v4l2_exportbuffer expbuf;
  GstVideoInfo *info = &pool->info;
  GstStructure *config;
  GstCaps *caps;

  newbuf = gst_buffer_new ();
  meta = GST_V4L2TRANS_META_ADD (newbuf);

  index = pool->num_allocated;

  GST_DEBUG_OBJECT (pool, "Creating buffer %u, %p for pool %p", index, newbuf,
      pool);

  memset (&meta->vbuffer, 0, sizeof meta->vbuffer);
  meta->vbuffer.index = index;
  meta->vbuffer.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
  meta->vbuffer.memory = V4L2_MEMORY_MMAP;

  /* Get a buffer from the driver */
  if (v4l2_ioctl (pool->v4l2_fd, VIDIOC_QUERYBUF, &meta->vbuffer) < 0)
    goto querybuf_failed;

  /* Export this buffer so it can be used as a DMABUF one */
  memset (&expbuf, 0, sizeof expbuf);
  expbuf.type = meta->vbuffer.type;
  expbuf.index = meta->vbuffer.index;
  expbuf.flags = O_CLOEXEC | O_RDWR;
  if (v4l2_ioctl (pool->v4l2_fd, VIDIOC_EXPBUF, &expbuf) < 0)
    goto expbuf_failed;

  GST_LOG_OBJECT (pool, "--index=%u  type=%d  bytesused=%u  flags=%08x"
      "  field=%d  memory=%d  MMAP offset=%u  fd=%d",
      meta->vbuffer.index, meta->vbuffer.type,
      meta->vbuffer.bytesused, meta->vbuffer.flags,
      meta->vbuffer.field, meta->vbuffer.memory,
      meta->vbuffer.m.offset, expbuf.fd);

  gst_buffer_append_memory (newbuf,
      gst_dmabuf_allocator_alloc (pool->allocator, expbuf.fd,
          meta->vbuffer.length));
  pool->num_allocated++;

  /* Queue this empty buffer */
  meta->vbuffer.bytesused = 0;
  if (v4l2_ioctl (pool->v4l2_fd, VIDIOC_QBUF, &meta->vbuffer) < 0)
    goto queue_failed;

  meta->v4l2_dequeued = FALSE;

  GST_DEBUG_OBJECT (pool, "Queued output buffer index %d", meta->vbuffer.index);

  gst_buffer_add_video_meta_full (newbuf, GST_VIDEO_FRAME_FLAG_NONE,
      GST_VIDEO_INFO_FORMAT (info), GST_VIDEO_INFO_WIDTH (info),
      GST_VIDEO_INFO_HEIGHT (info), GST_VIDEO_INFO_N_PLANES (info),
      info->offset, info->stride);

  *buffer = newbuf;

  /* Allocation on-demand is not supported:
   * if no buffers are available in pool, pool must wait for
   * buffer release. To trig this behaviour, max limit
   * must be set in pool config after all buffers are allocated.
   */
  if (pool->num_allocated >= pool->num_buffers) {
    config = gst_buffer_pool_get_config (bpool);
    gst_buffer_pool_config_get_params (config, &caps, &size,
        &min_buffers, &max_buffers);
    gst_buffer_pool_config_set_params (config, caps, size,
        pool->num_buffers, pool->num_buffers);
  }

  return GST_FLOW_OK;

  /* ERRORS */
querybuf_failed:
  {
    GST_WARNING ("Failed QUERYBUF");
    gst_buffer_unref (newbuf);
    return GST_FLOW_ERROR;
  }
expbuf_failed:
  {
    GST_WARNING ("Failed EXPBUF");
    gst_buffer_unref (newbuf);
    return GST_FLOW_ERROR;
  }
queue_failed:
  {
    GST_WARNING_OBJECT (pool, "Could not queue a buffer");
    gst_buffer_unref (newbuf);
    return GST_FLOW_ERROR;
  }
}

static gboolean
gst_v4l2_out_buf_pool_set_config (GstBufferPool * bpool, GstStructure * config)
{
  GstV4L2OutBufPool *pool = GST_V4L2_OUT_BUF_POOL (bpool);
  GstV4L2Trans *trans = pool->trans;
  GstCaps *caps;
  GstVideoInfo info;
  GstAllocator *allocator;
  GstAllocationParams params;
  GstVideoAlignment align;
  struct v4l2_requestbuffers reqbufs;
  struct v4l2_format s_fmt;
  struct v4l2_format g_fmt;
  struct v4l2_crop crop;
  struct v4l2_cropcap cropcap;
  struct v4l2_control ctrl;
  guint size, min_buf, max_buf, num_buf, aligned_width, aligned_height;
  gint ret;
  gboolean need_alignment;

  GST_DEBUG_OBJECT (pool, "Configuring %" GST_PTR_FORMAT, config);

  if (!gst_buffer_pool_config_get_params (config, &caps, &size,
          &min_buf, &max_buf))
    goto wrong_config;

  if (caps == NULL)
    goto no_caps;

  if (!gst_buffer_pool_config_get_allocator (config, &allocator, &params))
    goto wrong_config;

  /* parse the caps from the config */
  if (!gst_video_info_from_caps (&info, caps))
    goto wrong_caps;

  /* parse extra alignment info */
  need_alignment = gst_buffer_pool_config_has_option (config,
      GST_BUFFER_POOL_OPTION_VIDEO_ALIGNMENT);

  if (need_alignment)
    gst_buffer_pool_config_get_video_alignment (config, &align);
  else
    gst_video_alignment_reset (&align);

  /* add the padding */
  aligned_width = GST_VIDEO_INFO_WIDTH (&info) + align.padding_left +
      align.padding_right;
  aligned_height = GST_VIDEO_INFO_HEIGHT (&info) + align.padding_top +
      align.padding_bottom;

  pool->info = info;

  /* Configure V4L2 output */
  memset (&s_fmt, 0, sizeof s_fmt);
  s_fmt.fmt.pix.width = aligned_width;
  s_fmt.fmt.pix.height = aligned_height;
  s_fmt.fmt.pix.sizeimage = size;
  s_fmt.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;

  switch (GST_VIDEO_INFO_FORMAT (&info)) {
    case GST_VIDEO_FORMAT_RGB:
      s_fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_RGB24;
      s_fmt.fmt.pix.bytesperline = aligned_width * 3;
      break;
    case GST_VIDEO_FORMAT_NV12:
      s_fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_NV12;
      s_fmt.fmt.pix.bytesperline = aligned_width;
      break;
    case GST_VIDEO_FORMAT_I420:
      s_fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YUV420;
      s_fmt.fmt.pix.bytesperline = aligned_width;
      break;
#ifdef V4L2_PIX_FMT_XBGR32
    case GST_VIDEO_FORMAT_BGRx:
      s_fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_XBGR32;
      s_fmt.fmt.pix.bytesperline = aligned_width * 4;
      break;
#endif
    case GST_VIDEO_FORMAT_BGRA:
      s_fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_ABGR32;
      s_fmt.fmt.pix.bytesperline = aligned_width * 4;
      break;
    case GST_VIDEO_FORMAT_RGB16:
      s_fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_RGB565;
      s_fmt.fmt.pix.bytesperline = aligned_width * 2;
      break;
    default:
      goto wrong_config;
      break;
  }

  /* Format */
  ret = v4l2_ioctl (pool->v4l2_fd, VIDIOC_S_FMT, &s_fmt);
  if (ret != 0)
    goto error_s_fmt;

  /* Flip */
  memset (&ctrl, 0, sizeof ctrl);
  ctrl.id = V4L2_CID_HFLIP;
  ctrl.value = trans->hflip;
  ret = v4l2_ioctl (pool->v4l2_fd, VIDIOC_S_CTRL, &ctrl);
  if (ret != 0)
    goto error_flip;

  ctrl.id = V4L2_CID_VFLIP;
  ctrl.value = trans->vflip;
  ret = v4l2_ioctl (pool->v4l2_fd, VIDIOC_S_CTRL, &ctrl);
  if (ret != 0)
    goto error_flip;

  /* Crop if needed */
  if (align.padding_left || align.padding_right ||
      align.padding_top || align.padding_bottom) {
    memset (&cropcap, 0, sizeof cropcap);
    cropcap.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;

    ret = v4l2_ioctl (pool->v4l2_fd, VIDIOC_CROPCAP, &cropcap);
    if (ret != 0)
      goto error_s_crop;

    memset (&crop, 0, sizeof crop);
    crop.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
    crop.c.left = align.padding_left;
    crop.c.top = align.padding_top;
    crop.c.width = GST_VIDEO_INFO_WIDTH (&info);
    crop.c.height = GST_VIDEO_INFO_HEIGHT (&info);

    ret = v4l2_ioctl (pool->v4l2_fd, VIDIOC_S_CROP, &crop);
    if (ret != 0)
      goto error_s_crop;
  }

  memset (&g_fmt, 0, sizeof g_fmt);
  g_fmt.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
  ret = v4l2_ioctl (pool->v4l2_fd, VIDIOC_G_FMT, &g_fmt);
  if (ret != 0)
    goto error_g_fmt;

  GST_DEBUG_OBJECT (trans,
      "Format from V4L2 : fmt %.4s, width:%d, height:%d, bytesperline:%d "
      "sizeimage:%d",
      (char *) &g_fmt.fmt.pix.pixelformat,
      g_fmt.fmt.pix.width,
      g_fmt.fmt.pix.height, g_fmt.fmt.pix.bytesperline,
      g_fmt.fmt.pix.sizeimage);

  /* check that driver accepted the format parameters without changes */
  if ((s_fmt.fmt.pix.sizeimage != g_fmt.fmt.pix.sizeimage) ||
      (s_fmt.fmt.pix.bytesperline != g_fmt.fmt.pix.bytesperline)) {
    GST_WARNING_OBJECT (pool, "The driver change the format parameters");
  }

  if (need_alignment) {
    /* add pool option alignment */
    if (!gst_buffer_pool_config_has_option (config,
            GST_BUFFER_POOL_OPTION_VIDEO_ALIGNMENT))
      gst_buffer_pool_config_add_option (config,
          GST_BUFFER_POOL_OPTION_VIDEO_ALIGNMENT);

    /* add pool option metadata */
    if (!gst_buffer_pool_config_has_option (config,
            GST_BUFFER_POOL_OPTION_VIDEO_META))
      gst_buffer_pool_config_add_option (config,
          GST_BUFFER_POOL_OPTION_VIDEO_META);

    /* save align to transformer info & pool config */
    gst_video_info_align (&pool->info, &align);
    gst_buffer_pool_config_set_video_alignment (config, &align);

    GST_LOG_OBJECT (pool, "Padding %u-%ux%u-%u",
        align.padding_top, align.padding_left,
        align.padding_right, align.padding_bottom);
  }

  /* On-demand allocation is not supported, so max=min in config.
   * if max is specified, max buffers must be allocated */
  if (max_buf != 0)
    min_buf = max_buf;
  num_buf = min_buf;

  /* Request num_buf to V4L2 */
  if (pool->num_buffers) {
    /* Release existing buffers */
    memset (&reqbufs, 0, sizeof reqbufs);
    reqbufs.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
    reqbufs.count = 0;
    reqbufs.memory = V4L2_MEMORY_MMAP;
    if (v4l2_ioctl (pool->v4l2_fd, VIDIOC_REQBUFS, &reqbufs) < 0)
      goto error_ioc_reqbufs;
  }

  memset (&reqbufs, 0, sizeof reqbufs);
  reqbufs.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
  reqbufs.count = num_buf;
  reqbufs.memory = V4L2_MEMORY_MMAP;
  if (v4l2_ioctl (pool->v4l2_fd, VIDIOC_REQBUFS, &reqbufs) < 0)
    goto error_ioc_reqbufs;

  /* V4L2 could request a size image bigger than request
   * adjust size (read size image from g_fmt) and reinject it in pool config */
  size = g_fmt.fmt.pix.sizeimage;

  gst_buffer_pool_config_set_params (config, caps, size, num_buf, num_buf);

  pool->num_buffers = num_buf;

  /* Add dmabuf allocator */
  allocator = gst_dmabuf_allocator_new ();
  if (pool->allocator)
    gst_object_unref (pool->allocator);
  if ((pool->allocator = allocator))
    gst_object_ref (allocator);

  return GST_BUFFER_POOL_CLASS (parent_class)->set_config (bpool, config);

  /* ERRORS */
error_s_fmt:
  {
    GST_ERROR_OBJECT (pool, "Unable to set output format");
    return FALSE;
  }
error_flip:
  {
    GST_ERROR_OBJECT (pool, "Unable to set flip");
    return FALSE;
  }
error_s_crop:
  {
    GST_ERROR_OBJECT (pool, "Unable to set crop");
    return FALSE;
  }
error_g_fmt:
  {
    GST_ERROR_OBJECT (pool, "Unable to get output format");
    return FALSE;
  }
wrong_config:
  {
    GST_ERROR_OBJECT (pool, "Invalid config %" GST_PTR_FORMAT, config);
    return FALSE;
  }
no_caps:
  {
    GST_WARNING_OBJECT (pool, "No caps in config");
    return FALSE;
  }
wrong_caps:
  {
    GST_WARNING_OBJECT (pool,
        "Failed getting geometry from caps %" GST_PTR_FORMAT, caps);
    return FALSE;
  }
error_ioc_reqbufs:
  {
    GST_ERROR_OBJECT (pool, "Unable to request buffers");
    return FALSE;
  }
}

static gboolean
gst_v4l2_out_buf_pool_start (GstBufferPool * bpool)
{
  GstV4L2OutBufPool *pool = GST_V4L2_OUT_BUF_POOL (bpool);
  gint type;

  GST_DEBUG_OBJECT (pool, "Starting");

  pool->num_allocated = 0;

  /* allocate the buffers */
  if (!GST_BUFFER_POOL_CLASS (parent_class)->start (bpool))
    goto start_failed;

  /* Start streaming on output */
  type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
  if (v4l2_ioctl (pool->v4l2_fd, VIDIOC_STREAMON, &type) < 0)
    goto error_ioc_streamon;

  return TRUE;

  /* ERRORS */
start_failed:
  {
    GST_ERROR_OBJECT (pool, "Failed to start pool");
    return FALSE;
  }
error_ioc_streamon:
  {
    GST_ERROR_OBJECT (pool, "Streamon failed");
    return FALSE;
  }
}

static gboolean
gst_v4l2_out_buf_pool_stop (GstBufferPool * bpool)
{
  GstV4L2OutBufPool *pool = GST_V4L2_OUT_BUF_POOL (bpool);
  gboolean ret;
  gint type;

  GST_DEBUG_OBJECT (pool, "Stopping");

  type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
  if (v4l2_ioctl (pool->v4l2_fd, VIDIOC_STREAMOFF, &type) < 0)
    goto stop_failed;

  /* free the buffers in the queue */
  ret = GST_BUFFER_POOL_CLASS (parent_class)->stop (bpool);

  if (pool->num_buffers > 0) {
    struct v4l2_requestbuffers reqb;
    memset (&reqb, 0, sizeof reqb);

    reqb.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
    reqb.count = 0;
    reqb.memory = V4L2_MEMORY_MMAP;
    if (v4l2_ioctl (pool->v4l2_fd, VIDIOC_REQBUFS, &reqb) < 0) {
      GST_ERROR_OBJECT (pool, "Error releasing buffers");
    }
    pool->num_buffers = 0;
  }

  return ret;

  /* ERRORS */
stop_failed:
  {
    GST_ERROR_OBJECT (pool, "Error with STREAMOFF");
    return FALSE;
  }

}

static GstFlowReturn
gst_v4l2_out_buf_pool_acquire_buffer (GstBufferPool * bpool,
    GstBuffer ** buffer, GstBufferPoolAcquireParams * params)
{
  GstV4L2OutBufPool *pool = GST_V4L2_OUT_BUF_POOL (bpool);
  GstBuffer *outbuf;
  GstFlowReturn ret;
  GstV4L2TransMeta *meta;
  struct v4l2_buffer vbuffer;

  GST_DEBUG_OBJECT (pool, "Acquiring buffer");

  /* Get a released buffer by calling the parent class acquire_buffer.
   * This call is blocking and returns when the pool has an available buffer */
  ret = GST_BUFFER_POOL_CLASS (parent_class)->acquire_buffer (bpool,
      &outbuf, params);
  if (ret)
    goto no_acquire;

  /* Check if this acquired buffer is already dequeued : this happens if the
   * upstream element acquired a buffer and decided to release it without any
   * processing (the processing queues back the buffer).
   * If this is the case we will not dequeue this buffer again */
  meta = GST_V4L2TRANS_META_GET (outbuf);
  if (!meta)
    goto unknown_buffer;

  if (meta->v4l2_dequeued) {
    /* This buffer does not need to be dequeued */
    GST_DEBUG_OBJECT (pool, "Reusing dequeued buffer %p index %d",
        outbuf, meta->vbuffer.index);
  } else {
    /* Dequeue a free V4L2 buffer from driver */
    memset (&vbuffer, 0x00, sizeof (vbuffer));
    vbuffer.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;
    vbuffer.memory = V4L2_MEMORY_MMAP;

    if (v4l2_ioctl (pool->v4l2_fd, VIDIOC_DQBUF, &vbuffer) < 0)
      goto error;

    meta->v4l2_dequeued = TRUE;

    /* Update meta of the buffer, to used at the next QBUF */
    meta->vbuffer = vbuffer;

    GST_DEBUG_OBJECT (pool,
        "Dequeued output buffer %p, index =%d", outbuf, vbuffer.index);
  }

  *buffer = outbuf;

  return ret;

  /* ERRORS */
error:
  {
    GST_WARNING_OBJECT (pool,
        "Problem dequeuing frame %d (index=%d), pool-ct=%d, buf.flags=%d",
        vbuffer.sequence, vbuffer.index,
        GST_MINI_OBJECT_REFCOUNT (pool), vbuffer.flags);
    return GST_FLOW_ERROR;
  }

unknown_buffer:
  {
    GST_ERROR_OBJECT (pool,
        "The acquired buffer %p is not a known one", outbuf);
    return GST_FLOW_ERROR;
  }

no_acquire:
  {
    if (ret == GST_FLOW_FLUSHING)
      GST_WARNING_OBJECT (pool, "Flushing, cannot get buffer");
    else
      GST_ERROR_OBJECT (pool, "Cannot acquire buffer");
    return ret;
  }
}

GstFlowReturn
gst_v4l2_out_buf_pool_process (GstBufferPool * bpool, GstBuffer * buf)
{
  GstV4L2OutBufPool *pool = GST_V4L2_OUT_BUF_POOL (bpool);
  GstV4L2TransMeta *meta;

  GST_DEBUG_OBJECT (pool, "Processing output buffer");

  meta = GST_V4L2TRANS_META_GET (buf);
  if (!meta)
    goto no_meta;

  meta->vbuffer.bytesused = gst_buffer_get_size (buf);

  if (v4l2_ioctl (pool->v4l2_fd, VIDIOC_QBUF, &meta->vbuffer))
    goto error_ioctl_qbuf;

  meta->v4l2_dequeued = FALSE;

  GST_DEBUG_OBJECT (pool, "Queued output buffer %p, index %d",
      buf, meta->vbuffer.index);

  return GST_FLOW_OK;

  /* ERRORS */
no_meta:
  {
    GST_ERROR_OBJECT (pool, "No meta attached to buffer");
    return GST_FLOW_ERROR;
  }
error_ioctl_qbuf:
  {
    GST_ERROR_OBJECT (pool, "QBUF failed");
    return GST_FLOW_ERROR;
  }
}

static void
gst_v4l2_out_buf_pool_finalize (GObject * object)
{
  GstV4L2OutBufPool *pool = GST_V4L2_OUT_BUF_POOL (object);

  if (pool->v4l2_fd > 0)
    v4l2_close (pool->v4l2_fd);
  if (pool->allocator)
    gst_object_unref (pool->allocator);
  gst_object_unref (pool->trans);

  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gst_v4l2_out_buf_pool_init (GstV4L2OutBufPool * pool)
{
}

static void
gst_v4l2_out_buf_pool_class_init (GstV4L2OutBufPoolClass * klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GstBufferPoolClass *bufferpool_class = GST_BUFFER_POOL_CLASS (klass);

  object_class->finalize = gst_v4l2_out_buf_pool_finalize;

  bufferpool_class->start = gst_v4l2_out_buf_pool_start;
  bufferpool_class->stop = gst_v4l2_out_buf_pool_stop;
  bufferpool_class->get_options = gst_v4l2_out_buf_pool_get_options;
  bufferpool_class->set_config = gst_v4l2_out_buf_pool_set_config;
  bufferpool_class->alloc_buffer = gst_v4l2_out_buf_pool_alloc_buffer;
  bufferpool_class->acquire_buffer = gst_v4l2_out_buf_pool_acquire_buffer;
}

GstBufferPool *
gst_v4l2_out_buf_pool_new (GstV4L2Trans * trans)
{
  GstV4L2OutBufPool *pool;
  gint v4l2_fd;

  GST_DEBUG_OBJECT (trans, "Creating output bufferpool");

  v4l2_fd = v4l2_dup (trans->fd);
  if (v4l2_fd < 0) {
    GST_DEBUG ("Failed to dup fd");
    return NULL;
  }

  pool = (GstV4L2OutBufPool *)
      g_object_new (GST_TYPE_V4L2_OUT_BUF_POOL, NULL);

  /* take a reference on v4l2trans to be sure it is released after the pool */
  pool->trans = gst_object_ref (trans);
  pool->v4l2_fd = v4l2_fd;

  return GST_BUFFER_POOL (pool);
}
