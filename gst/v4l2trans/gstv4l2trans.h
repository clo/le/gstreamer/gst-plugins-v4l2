/*
* Copyright (C) 2015 STMicroelectronics SA
*
* Author: <fabien.dessenne@st.com> for STMicroelectronics.
*
*/
#ifndef  GSTV4L2TRANS_H
#define  GSTV4L2TRANS_H

#include <gst/gst.h>
#include <gst/video/gstvideofilter.h>

#include <linux/videodev2.h>

#ifndef V4L2_PIX_FMT_ABGR32
#define V4L2_PIX_FMT_ABGR32  v4l2_fourcc('A', 'R', '2', '4') /* 32  BGRA-8-8-8-8  */
#endif

#ifdef HAVE_LIBV4L2
#include <libv4l2.h>
#else
#include <unistd.h>
#include <sys/ioctl.h>
#define v4l2_fd_open(fd, flags) (fd)
#define v4l2_close    close
#define v4l2_dup      dup
#define v4l2_ioctl    ioctl
#define v4l2_read     read
#define v4l2_mmap     mmap
#define v4l2_munmap   munmap
#endif

/* Number of buffers in capture pool  */
#define NB_BUF_CAPTURE_POOL 3

/* Begin Declaration */
G_BEGIN_DECLS
#define GST_TYPE_V4L2TRANS (gst_v4l2trans_get_type())
#define GST_V4L2TRANS(obj) \
    (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_TYPE_V4L2TRANS, GstV4L2Trans))
#define GST_V4L2TRANS_CLASS(klass) \
    (G_TYPE_CHECK_CLASS_CAST((klass), GST_TYPE_V4L2TRANS, GstV4L2TransClass))
#define GST_IS_V4L2TRANS(obj) \
    (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_TYPE_V4L2TRANS))
#define GST_IS_V4L2TRANS_CLASS(obj) \
    (G_TYPE_CHECK_CLASS_TYPE((klass), GST_TYPE_V4L2TRANS))
typedef struct _GstV4L2Trans GstV4L2Trans;
typedef struct _GstV4L2TransClass GstV4L2TransClass;

/**
 * output_no_pool:
 * @mmap_virt:       Array of pointers to frames
 * @mmap_size:       Array of pointer sizes of frames
 * @current_nb_buf:  Frames array size
 * @setup:           Is configured
 *
 */
struct output_no_pool
{
  void **mmap_virt;
  gint *mmap_size;
  gint current_nb_buf;
  gboolean setup;
};

/**
 * _GstV4L2Trans:
 * @parent:                Element parent
 * @fd:                    V4L2 driver file descriptor
 * @output_pool:           Pool for incoming frames. We own this pool and share
 *                         it with the upstream element.
 * @out_no_pool:           Memory buffer info. Needed when our proposed output
 *                         pool is not used by the upstream element.
 * @capture_pool:          Pool for outgoing frames. We own this pool and share
 *                         it with the downstream element.
 * @capture_down_pool:     Pool for outgoing frames. The downstream element own
 *                         this pool and share it with us.
 * @capture_start:         Is the capture started
 * @capture_v4l2_fd:       Correspondence table between a v4l2 index and a fd
 * @hflip:                 Horizontal flip
 * @vflip:                 Vertical flip
 */
struct _GstV4L2Trans
{
  GstVideoFilter parent;
  gint fd;

  GstBufferPool *output_pool;
  struct output_no_pool out_no_pool;

  GstBufferPool *capture_pool;
  GstBufferPool *capture_down_pool;
  gboolean capture_start;
  gint capture_v4l2_fd[NB_BUF_CAPTURE_POOL];

  gboolean hflip;
  gboolean vflip;
};

struct _GstV4L2TransClass
{
  GstVideoFilterClass parent_class;
};

GType gst_v4l2trans_get_type (void);

G_END_DECLS
#endif /* GSTV4L2TRANS_H */
